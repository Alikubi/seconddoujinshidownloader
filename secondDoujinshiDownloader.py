
"""
Created by alice on 2016/7/29
"""

import urllib.request,sys,time,shutil,os
from bs4 import BeautifulSoup

URL = "http://www.177pic.info/html/category/jj"
USERAGENT = "Mozilla/5.0"

CURRENTPATH = os.getcwd()

# 進捗状況報告
def progress(block_count,block_size,total_size):    
    percentage = 100.0 * block_count * block_size / total_size
    sys.stdout.write('%.2f %% ( %d KB)\r' % (percentage,total_size/1024))

# スープ作り
def making_soup(url):
    req = urllib.request.Request(url)
    req.add_header('User-agent',USERAGENT)
    try:
        res = urllib.request.urlopen(req)
    except urllib.error.HTTPError:
        time.sleep(10)
        res = urllib.request.urlopen(req)
    html = res.read()
    soup = BeautifulSoup(html,'html.parser')
    return soup

# 各ページの処理
def handle_page(url):
    html = making_soup(url)
    for h2 in html.findAll('h2'):
        for each_content_url in h2.findAll('a'):
            each_content(dict(each_content_url.attrs)['href'])

# topページ
def top(url):
    handle_page(url)
    html = making_soup(url)
    search_next_page(html)

# 次のページ
def search_next_page(url):
    for page_num in url.findAll('div',attrs={'class':'page_num'}):
        for each_page_url in page_num.findAll('a'):
            next_number = int(each_page_url.text)
            if next_number > 1:
                print (next_number)
                each_page(dict(each_page_url.attrs)['href'])

# それぞれのページでの処理
def each_page(url):
    handle_page(url)

# それぞれの同人誌にアクセス
def each_content(url):
    start_number = 1
    html = making_soup(url)
    title = html.title.text
    print ("Please Wait")
    time.sleep(5)
    for image_url in html.findAll('img'):
        img = (dict(image_url.attrs)['src'])
        img_title = (dict(image_url.attrs)['src'].split("/"))[-1]
        path = make_directory(title)
        download(img,img_title,path,title)
    for wp_pagenavi in html.findAll('div',attrs={'class':'wp-pagenavi'}):
        for next_url in wp_pagenavi.findAll('a'):
            try:
                order_number = int(next_url.text)
            except ValueError as e:
                pass
            # 次のリンクに飛んで欲しいから
            # 強引に
            if start_number < order_number:
                next_page_of_content(dict(next_url.attrs)['href'],title)
                start_number = order_number

# それぞれの同人誌の次のページ
def next_page_of_content(url,title):
    html = making_soup(url)
    time.sleep(2)
    for a in html.findAll('img'):
        img_title = (dict(a.attrs)['src'].split("/"))[-1]
        img = dict(a.attrs)['src']
        path = make_directory(title)
        download(img,img_title,path,title)

# ディレクトリ作成
def make_directory(foldername):
    path = CURRENTPATH + "/" + foldername
    if os.path.isdir(path):
        pass
    else:
        os.mkdir(path)
    return path

# ダウンロード
def download(content,content_name,path,title):
    print ("Download")
    print ("Title: {}".format(title))
    print ("URL: {}".format(content))
    print ("Content: {}".format(content_name))
    if content_name in os.listdir(path):
        print ("Already exists")
    else:
        opener = urllib.request.build_opener()
        opener.addheaders = [('User-agent','Mozila/5.0')]
        urllib.request.install_opener(opener)
        try:
            urllib.request.urlretrieve(content,content_name,progress)
            shutil.move(content_name,path)
        # Not Foundに対応
        except urllib.error.HTTPError as e:
            print (e)
        except shutil.Error:
            pass

# main関数
if __name__ == "__main__":
    top(URL)

